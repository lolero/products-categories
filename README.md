# products-categories

## Description
A sample product and category browser application

The application is composed by a self-contained microservice 'api' module and
a rudimentary front-end for making viewing products and descriptions, as well as
making CRUD operations on both entities.

To run the api microservice, open a terminal, navigate to the api directory, and run:

`java -jar products-categories.jar`

To run the front-end application, open a different terminal, navigate to the web directory, and run:

`npm install`

`npm start`