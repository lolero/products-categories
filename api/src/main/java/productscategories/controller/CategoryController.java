package productscategories.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import productscategories.domain.Category;
import productscategories.service.CategoryService;
import productscategories.service.MapValidationErrorService;

import javax.validation.Valid;
import java.util.HashSet;

@RestController
@RequestMapping("/categories")
@CrossOrigin
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private MapValidationErrorService mapValidationErrorService;

    @PostMapping("")
    public ResponseEntity<?> createNewCategory(@Valid @RequestBody Category category, BindingResult result) {
        ResponseEntity<?> errorMap = mapValidationErrorService.mapValidationService(result);
        if(errorMap!=null) return errorMap;

        Category savedCategory = categoryService.saveOrUpdate(category);
        return new ResponseEntity<>(savedCategory, HttpStatus.CREATED);
    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<?> getCategoryById(@PathVariable int categoryId) {

        Category category = categoryService.findById(categoryId);

        return new ResponseEntity<Category>(category, HttpStatus.OK);
    }

    @GetMapping("")
    public Iterable<Category> getAllCategories() {
        Iterable<Category> categories = categoryService.findAll();
        categories.forEach(category -> category.getProducts().forEach(product ->
            product.setCategories(new HashSet<>())));
        return categories;
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<String> deleteCategory(@PathVariable int categoryId) {
        categoryService.delete(categoryId);

        return new ResponseEntity<>("Category with ID '" + categoryId + "' was successfully deleted", HttpStatus.OK);
    }
}
