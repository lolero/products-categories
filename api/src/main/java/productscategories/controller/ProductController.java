package productscategories.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import productscategories.domain.Product;
import productscategories.service.ProductService;
import productscategories.service.MapValidationErrorService;

import javax.validation.Valid;
import java.util.HashSet;

@RestController
@RequestMapping("/products")
@CrossOrigin
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private MapValidationErrorService mapValidationErrorService;

    @PostMapping("")
    public ResponseEntity<?> createNewProduct(@Valid @RequestBody Product product, BindingResult result) {
        ResponseEntity<?> errorMap = mapValidationErrorService.mapValidationService(result);
        if(errorMap!=null) return errorMap;

        Product savedProduct = productService.saveOrUpdate(product);
        return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<?> getProductById(@PathVariable int productId) {

        Product product = productService.findById(productId);

        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @GetMapping("")
    public Iterable<Product> getAllProducts() {
        Iterable<Product> products = productService.findAll();
        products.forEach(product -> product.getCategories().forEach(category ->
               category.setProducts(new HashSet<>())));
        return products;
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable int productId) {
        productService.delete(productId);

        return new ResponseEntity<>("Product with ID '" + productId + "' was successfully deleted", HttpStatus.OK);
    }
}
