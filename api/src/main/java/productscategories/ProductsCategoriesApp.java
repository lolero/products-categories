package productscategories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductsCategoriesApp {
    public static void main(String[] args) {
        SpringApplication.run(ProductsCategoriesApp.class, args);
    }
}
