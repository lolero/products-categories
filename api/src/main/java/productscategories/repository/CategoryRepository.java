package productscategories.repository;

import org.springframework.data.repository.CrudRepository;
import productscategories.domain.Category;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
}
