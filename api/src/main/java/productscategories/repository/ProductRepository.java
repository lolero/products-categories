package productscategories.repository;

import org.springframework.data.repository.CrudRepository;
import productscategories.domain.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
