package productscategories.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import productscategories.domain.Category;
import productscategories.exception.CategoryIdException;
import productscategories.exception.CategoryNameException;
import productscategories.repository.CategoryRepository;

import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Category saveOrUpdate(Category category) {
        try {
            return categoryRepository.save(category);
        } catch (Exception e) {
            throw new CategoryNameException("Category name '" + category.getName() + "' already exists");
        }
    }

    public Category findById(int categoryId) {
        Optional<Category> category = categoryRepository.findById(categoryId);

        if (!category.isPresent()) {
            throw new CategoryIdException("Category ID '" + categoryId + "' does not exist");
        }

        return category.get();
    }

    public Iterable<Category> findAll() {
        return categoryRepository.findAll();
    }

    public void delete(int categoryId) {
        Optional<Category> category = categoryRepository.findById(categoryId);

        if (!category.isPresent()) {
            throw new CategoryIdException("Category ID '" + categoryId + "' does not exist");
        }

        categoryRepository.delete(category.get());
    }
}
