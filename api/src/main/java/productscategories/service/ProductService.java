package productscategories.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import productscategories.domain.Product;
import productscategories.exception.ProductIdException;
import productscategories.repository.ProductRepository;

import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product saveOrUpdate(Product product) {
        return productRepository.save(product);
    }

    public Product findById(int productId) {
        Optional<Product> product = productRepository.findById(productId);

        if (!product.isPresent()) {
            throw new ProductIdException("Product ID '" + productId + "' does not exist");
        }

        return product.get();
    }

    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    public void delete(int productId) {
        Optional<Product> product = productRepository.findById(productId);

        if (!product.isPresent()) {
            throw new ProductIdException("Product ID '" + productId + "' does not exist");
        }

        productRepository.delete(product.get());
    }
}
