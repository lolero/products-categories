import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import store from '../modules/dashboard/redux/store';
import Dashboard from './components/DemoDashboard/DemoDashboard';


const main = () => {
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <Dashboard />
      </BrowserRouter>
    </Provider>,
    document.getElementById('root')
  );
};

main();