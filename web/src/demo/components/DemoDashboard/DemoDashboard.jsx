import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Link, Route, Switch, withRouter, matchPath } from 'react-router-dom';
import { Icon, Menu } from 'semantic-ui-react';

import { Dashboard } from '../../../modules/dashboard/dashboard';
import { Categories } from '../../../modules/categories/categories';
import { Products } from '../../../modules/products/products';

class DemoDashboard extends Component {

  isMatch = path => (
    matchPath(
      this.props.location.pathname,
      {
        path,
        exact: false,
        strict: false,
      }
    ) !== null
  );

  renderNavMenu = (options = {}) => (
    <Menu.Menu className={options.menuClassName}>
      <Menu.Item
        className={options.itemClassName}
        active={this.isMatch('/products')}
        as={Link}
        to="/products"
      >
        <Icon name="list" />
        Products
      </Menu.Item>
      <Menu.Item
        className={options.itemClassName}
        active={this.isMatch('/categories')}
        as={Link}
        to="/categories"
      >
        <Icon name="sitemap" />
        Categories
      </Menu.Item>
    </Menu.Menu>
  );

  renderContent = (options = {}) => (
    <Switch>
      <Route path="/products" component={Products} />
      <Route path="/categories" component={Categories} />
    </Switch>
  );

  render() {
    return (
      <Dashboard
        renderNavMenu={this.renderNavMenu}
        renderContent={this.renderContent}
      />
    );
  }
}

DemoDashboard.propTypes = {
  location: PropTypes.shape({ pathname: PropTypes.string }).isRequired,
};

export default withRouter(DemoDashboard);
