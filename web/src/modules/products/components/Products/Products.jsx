import React from 'react';
import {Route, Switch} from 'react-router-dom';

import ProductsHome from '../ProductsHome/ProductsHome';
import ProductAddEdit from '../ProductAddEdit/ProductAddEdit';

const Products = () =>(
  <Switch>
    <Route exact path="/products" component={ProductsHome} />
    <Route exact path="/products/new" component={ProductAddEdit} />
    <Route exact path="/products/edit/:id" component={ProductAddEdit} />
  </Switch>
);

export default Products;