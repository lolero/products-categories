export const validateProduct = (fieldName, currentValue) => {
  const errors = [];
  switch (fieldName) {
    case 'price':
      if (isNaN(currentValue) || currentValue < 0) errors.push('Price must be a positive number');
      break;
    case 'categoryIds':
      if (currentValue.length < 1) errors.push('Please select at least one category');
      break;
    default: break;
  }

  return errors;
};