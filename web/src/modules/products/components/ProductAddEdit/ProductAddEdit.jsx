import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import {Button, Dropdown, Form, Header, Segment} from 'semantic-ui-react';

import FeedbackFormField from '../../../common/components/FeedbackFormField/FeedbackFormField';
import { postProduct } from '../../../common/services/productsServices';
import {
  fieldDataFeedbackType,
  fieldDataType,
  getNewFieldsData,
  newFieldsDataActionType,
} from '../../../common/helpers/fieldDataHelpers';
import connect from 'react-redux/es/connect/connect';
import { fetchProductsAndCategories } from '../../../common/redux/actions/appActions';
import { validateProduct } from './ProductAddEdit.helpers';

class ProductAddEdit extends Component {
  constructor(props) {
    super(props);
    const name = props.match.params.id ? props.products[props.match.params.id].name : '';
    const description = props.match.params.id ? props.products[props.match.params.id].description : '';
    const price = props.match.params.id ? props.products[props.match.params.id].price : '';
    const currency = props.match.params.id ? props.products[props.match.params.id].currency : 'EUR';
    const categoryIds = props.match.params.id ?
      props.products[props.match.params.id].categoryIds :
      [_.sortBy(
        Object.values(props.categories),
        category => category.name,
      )[0].id];

    const fieldsData = {
      name: {
        type: fieldDataType.NAME,
          savedValue: name,
          currentValue: name,
          feedback: [],
      },
      description: {
        type: fieldDataType.STRING,
          savedValue: description,
          currentValue: description,
          feedback: [],
      },
      price: {
        type: fieldDataType.NUMBER,
          savedValue: price,
          currentValue: price,
          feedback: [],
      },
      currency: {
        type: fieldDataType.STRING,
          savedValue: currency,
          currentValue: currency,
          feedback: [],
      },
      categoryIds: {
        type: fieldDataType.ARRAY,
          savedValue: categoryIds,
          currentValue: categoryIds,
          feedback: [],
      }
    };
    this.state = { fieldsData };
  };

  async saveProduct() {
    const fieldsData = getNewFieldsData(this.state.fieldsData, newFieldsDataActionType.PROCESSING);
    if (fieldsData) {
      this.setState({ fieldsData });
      if (this.props.match.params.id) fieldsData.id = { currentValue: this.props.match.params.id};
      const postFieldsData = {
        ...fieldsData,
        categories: {
          ...fieldsData.categoryIds,
          currentValue: fieldsData.categoryIds.currentValue.map(categoryId => ({ id: categoryId }))
        }
      };
      delete postFieldsData.categoryIds;
      const response = await postProduct(postFieldsData);
      if (response.status === 201) {
        this.props.fetchProductsAndCategories();
        this.props.history.push('/products');
      } else {
        this.setState(state => ({
          fieldsData: getNewFieldsData(
            state.fieldsData,
            newFieldsDataActionType.BACK_END_ERROR,
            response.data,
          )
        }));
      }
    }
  };

  onChange(fieldName, fieldData) {
    const errors = validateProduct(fieldName, fieldData.currentValue);
    const newFieldData = {
      ...fieldData,
      feedback: [
        ...fieldData.feedback,
        ...errors.map(error => ({
          type: fieldDataFeedbackType.FRONT_END_ERROR,
          message: error
        })),
      ],
    };
    const fieldsData = {
      ...this.state.fieldsData,
      [fieldName]: newFieldData
    };
    this.setState({ fieldsData });
  }

  render() {
    return (
      <Segment basic>
        <Header>{this.props.match.params.id ? 'Edit' : 'New'} Product</Header>
        <Form>
          {
            _.reject(Object.keys(this.state.fieldsData), fieldName =>
              ['id', 'currency', 'categoryIds'].includes(fieldName))
              .map(fieldName => (
                <FeedbackFormField
                  key={fieldName}
                  label={_.upperFirst(fieldName)}
                  name={fieldName}
                  data={this.state.fieldsData[fieldName]}
                  fluid={fieldName !== 'price'}
                  onChange={fieldData => this.onChange(fieldName, fieldData)}
                />
              ))
          }
          <Form.Field>
            <label>
              Currency
            </label>
            <Dropdown
              placeholder='Select Currency'
              fluid
              selection
              options={[
                { text: 'USD', value: 'USD' },
                { text: 'EUR', value: 'EUR' },
              ]}
              value={this.state.fieldsData.currency.currentValue}
              onChange={(e, data) => this.onChange('currency', {
                ...this.state.fieldsData.currency,
                currentValue: data.value,
              })}
            />
          </Form.Field>
          <Form.Field
            error={
              this.state.fieldsData.categoryIds.feedback.some(feedback => (
                feedback.type === fieldDataFeedbackType.FRONT_END_ERROR ||
                feedback.type === fieldDataFeedbackType.BACK_END_ERROR
              ))
            }
          >
            <label>
              Categories
            </label>
            <Dropdown
              placeholder='Categories'
              fluid
              multiple
              selection
              options={Object.values(this.props.categories).map(category => ({
                key: category.id,
                text: category.name,
                value: category.id,
              }))}
              value={this.state.fieldsData.categoryIds.currentValue}
              onChange={(e, data) => this.onChange('categoryIds', {
                ...this.state.fieldsData.categoryIds,
                currentValue: data.value,
              })}
            />
            {this.state.fieldsData.categoryIds.feedback.some(({ message }) => !!message) &&
              <label>
                {this.state.fieldsData.categoryIds.feedback.map(({ message }) => <p key={message}>{message}</p>)}
              </label>
            }
          </Form.Field>
          <Button
            primary
            disabled={
              _.reject(Object.keys(this.state.fieldsData), fieldName =>
                ['id', 'description', 'currency'].includes(fieldName)).some(fieldName => (
                  this.state.fieldsData[fieldName].feedback.length > 0 ||
                  this.state.fieldsData[fieldName].currentValue.length < 1
              ))
            }
            onClick={() => this.saveProduct()}
          >
            Submit
          </Button>
        </Form>
      </Segment>
    );
  }
}

ProductAddEdit.propTypes = {
  history: PropTypes.any.isRequired,
  match: PropTypes.object.isRequired,
  categories: PropTypes.object.isRequired,
  products: PropTypes.object.isRequired,
  fetchProductsAndCategories: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  categories: state.appReducer.categories,
  products: state.appReducer.products,
});

export default connect(
  mapStateToProps,
  {
    fetchProductsAndCategories,
  },
)(withRouter(ProductAddEdit));
