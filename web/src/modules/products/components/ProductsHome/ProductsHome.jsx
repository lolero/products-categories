import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {Button, Dropdown, Header, Icon, Item, Modal, Segment} from 'semantic-ui-react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { deleteProduct } from '../../../common/services/productsServices';
import { fetchProductsAndCategories } from '../../../common/redux/actions/appActions';

class ProductsHome extends Component {
  constructor(props) {
    super(props);
    const selectedCategoryId = _.isEmpty(props.categories) ?
      null :
      _.sortBy(
        Object.values(props.categories),
        category => category.name,
      )[0].id;

    this.state = { selectedCategoryId };
  }

  componentDidMount() {
    if (_.isEmpty(this.props.categories)) this.props.history.push('/categories/new');
  }

  createNewProduct() {
    this.props.history.push('/products/new');
  }

  editProduct(productId) {
    this.props.history.push(`/products/edit/${productId}`);
  }

  async deleteProduct(productId) {
    await deleteProduct(productId);
    this.props.fetchProductsAndCategories();
  }

  render() {
    return (
      <React.Fragment>
        <Segment basic>
          <Dropdown
            placeholder='Select Category'
            selection
            value={this.state.selectedCategoryId}
            options={Object.values(this.props.categories).map(category => ({
              text: category.name,
              value: category.id,
            }))}
            onChange={(e, data) => this.setState({ selectedCategoryId: data.value })}
          />
          <Button
            primary
            icon
            labelPosition='left'
            onClick={() => this.createNewProduct()}
          >
            <Icon name='plus' />
            New Product
          </Button>
        </Segment>
        <Segment basic>
          <Item.Group divided>
            {this.props.categories[this.state.selectedCategoryId] &&
            this.props.categories[this.state.selectedCategoryId].productIds.map(productId => (
              <Item key={this.props.products[productId].id}>
                <Item.Content>
                  <Item.Header>{this.props.products[productId].name}</Item.Header>
                  <Item.Meta>
                    <span>{_.upperCase(this.props.products[productId].currency)}</span>
                    <span>{this.props.products[productId].price}</span>
                  </Item.Meta>
                  <Item.Meta>
                    <span>Categories: </span>
                    {this.props.products[productId].categoryIds.map(categoryId => (
                      <span key={categoryId}>{this.props.categories[categoryId].name}, </span>
                    ))}
                  </Item.Meta>
                  <Item.Description>{this.props.products[productId].description}</Item.Description>
                  <Item.Extra>
                    <Button
                      icon
                      labelPosition='left'
                      onClick={() => this.editProduct(this.props.products[productId].id)}
                    >
                      <Icon name='pencil' />
                      Edit
                    </Button>
                    <Modal
                      trigger={
                        <Button icon labelPosition='left'>
                          <Icon name='delete' />
                          Delete
                        </Button>
                      }
                      size="mini"
                      closeIcon
                    >
                      <Header content='Delete Product' />
                      <Modal.Content>
                        Are you sure you want to delete product {this.props.products[productId].name}?
                      </Modal.Content>
                      <Modal.Actions>
                        <Button onClick={() => this.deleteProduct(this.props.products[productId].id)}>
                          Yes
                        </Button>
                      </Modal.Actions>
                    </Modal>
                  </Item.Extra>
                </Item.Content>
              </Item>
            ))}
          </Item.Group>
        </Segment>
      </React.Fragment>
    );
  }
}

ProductsHome.propTypes = {
  history: PropTypes.object.isRequired,
  categories: PropTypes.object.isRequired,
  products: PropTypes.object.isRequired,
  fetchProductsAndCategories: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  categories: state.appReducer.categories,
  products: state.appReducer.products,
});

export default connect(
  mapStateToProps,
  {
    fetchProductsAndCategories,
  },
)(withRouter((ProductsHome)));