import axios from "axios";
import {fieldsDataToPayload} from "../helpers/fieldDataHelpers";

export const getProducts = async () => {
  return axios({
    method: 'GET',
    url: 'http://localhost:8080/products',
    validateStatus: status => true,
  }).then(response => response);
};

export const postProduct = async (productFieldData) => {
  return axios({
    method: 'POST',
    url: 'http://localhost:8080/products',
    data: fieldsDataToPayload(productFieldData),
    validateStatus: status => true,
  }).then(response => response);
};

export const deleteProduct = async (productId) => {
  return axios({
    method: 'delete',
    url: `http://localhost:8080/products/${productId}`,
    validateStatus: status => true,
  }).then(response => response);
};