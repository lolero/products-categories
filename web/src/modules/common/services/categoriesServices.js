import axios from "axios";
import {fieldsDataToPayload} from "../helpers/fieldDataHelpers";

export const getCategories = async () => {
  return axios({
    method: 'GET',
    url: 'http://localhost:8080/categories',
    validateStatus: status => true,
  }).then(response => response);
};

export const postCategory = async (categoryFieldData) => {
  return axios({
    method: 'POST',
    url: 'http://localhost:8080/categories',
    data: fieldsDataToPayload(categoryFieldData),
    validateStatus: status => true,
  }).then(response => response);
};

export const deleteCategory = async (categoryId) => {
  return axios({
    method: 'delete',
    url: `http://localhost:8080/categories/${categoryId}`,
    validateStatus: status => true,
  }).then(response => response);
};