import PropTypes from "prop-types";
import _ from 'lodash';

export const fieldDataShape = PropTypes.shape({
  type: PropTypes.string,
  savedValue: PropTypes.any,
  currentValue: PropTypes.any,
  feedback: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.string,
    message: PropTypes.string,
  })),
});

export const fieldDataType = {
  NAME: 'NAME',
  STRING: 'STRING',
  NUMBER: 'NUMBER',
  ARRAY: 'ARRAY',
};

/**
 * Create new fieldsData object whose fields are either reverted to saved
 * values, their feedback from backend services is removed, or a feedback of
 * 'PROCESSING is added to them
 * @param {fieldsData} fieldsData object to be updated and returned
 * @param {action} One of 'PROCESSING', 'CANCEL' or 'REMOVE_SERVICE_FEEDBACK'
 * @return {object} Updated fieldsData object
 */
export const getNewFieldsData = (fieldsData, action, fieldMessages = null) => {
  let isFieldDataModified = false;
  const newFieldsData = _.mapValues(fieldsData, (fieldData, fieldName) => {
    switch (action) {
      case newFieldsDataActionType.CANCEL:
        if (!_.isEqual(fieldData.savedValue, fieldData.currentValue)) {
          isFieldDataModified = true;
          return {
            ...fieldData,
            currentValue: _.clone(fieldData.savedValue),
            feedback: [],
          };
        }
        return { ...fieldData };
      case newFieldsDataActionType.PROCESSING:
        if (!_.isEqual(fieldData.savedValue, fieldData.currentValue)) {
          isFieldDataModified = true;
          return {
            ...fieldData,
            feedback: [{type: fieldDataFeedbackType.PROCESSING}],
          };
        }
        return { ...fieldData };
      case newFieldsDataActionType.FRONT_END_ERROR:
        isFieldDataModified = true;
        if (!fieldMessages) {
          return {
            ...fieldData,
            feedback: [{ type: fieldDataFeedbackType.FRONT_END_ERROR }],
          };
        } else if (fieldMessages[fieldName]) {
          return {
            ...fieldData,
            feedback: [{
              type: fieldDataFeedbackType.FRONT_END_ERROR,
              message: fieldMessages[fieldName],
            }],
          };
        }
        return { ...fieldData, feedback: [] };
      case newFieldsDataActionType.BACK_END_ERROR:
        isFieldDataModified = true;
        if (!fieldMessages) {
          return {
            ...fieldData,
            feedback: [{ type: fieldDataFeedbackType.BACK_END_ERROR }],
          };
        } else if (fieldMessages[fieldName]) {
          return {
            ...fieldData,
            feedback: [{
              type: fieldDataFeedbackType.BACK_END_ERROR,
              message: fieldMessages[fieldName],
            }],
          };
        }
        return { ...fieldData, feedback: [] };
      case newFieldsDataActionType.SUCCESS:
        if (!_.isEqual(fieldData.savedValue, fieldData.currentValue)) {
          isFieldDataModified = true;
          return {
            ...fieldData,
            savedValue: _.clone(fieldData.currentValue),
            feedback: [{ type: fieldDataFeedbackType.SUCCESS }],
          };
        }
        return { ...fieldData };
      default: // REMOVE_SERVICE_FEEDBACK
        if (fieldData.feedback.some(({type}) => type === fieldDataFeedbackType.SUCCESS)) {
          isFieldDataModified = true;
          return {
            ...fieldData,
            feedback: fieldData.feedback.filter(feedbackItem => (
              feedbackItem.type === fieldDataFeedbackType.SUCCESS
            )),
          };
        }
        return { ...fieldData };
    }
  });
  return isFieldDataModified ? newFieldsData : null;
};

export const newFieldsDataActionType = {
  CANCEL: 'CANCEL',
  PROCESSING: 'PROCESSING',
  FRONT_END_ERROR: 'FRONT_END_ERROR',
  BACK_END_ERROR: 'BACK_END_ERROR',
  SUCCESS: 'SUCCESS',
  REMOVE_SERVICE_FEEDBACK: 'REMOVE_SERVICE_FEEDBACK',
};

export const fieldDataFeedbackType = {
  FRONT_END_ERROR: 'FRONT_END_ERROR',
  BACK_END_ERROR: 'BACK_END_ERROR',
  PROCESSING: 'PROCESSING',
  SUCCESS: 'SUCCESS',
};

export const fieldsDataToPayload = (fieldsData) => {
  return _.mapValues(fieldsData, fieldData => fieldData.currentValue);
};
