import { GET_PRODUCTS_AND_CATEGORIES } from './appActionTypes';
import { getProducts } from '../../services/productsServices';
import { getCategories } from '../../services/categoriesServices';

export const fetchProductsAndCategories = () => async dispatch => {
  const categoriesResponse = await getCategories();
  const productsResponse = await getProducts();
  dispatch({
    type: GET_PRODUCTS_AND_CATEGORIES,
    payload: {
      categories: categoriesResponse.data.reduce((categories, category) => {
        const categoryWithProductIds = {
          ...category,
          productIds: category.products.map(product => typeof product === 'object' ? product.id : product),
        };
        delete categoryWithProductIds.products;
        return {
          ...categories,
          [category.id]: categoryWithProductIds,
        };
      }, {}),
      products: productsResponse.data.reduce((products, product) => {
        const productWithCategoryIds = {
          ...product,
          categoryIds: product.categories.map(category => typeof category === 'object' ? category.id : category),
        };
        delete productWithCategoryIds.categories;
        return {
          ...products,
          [product.id]: productWithCategoryIds,
        }
      }, {}),
    },
  });
};