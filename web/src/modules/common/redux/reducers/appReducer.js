import { GET_PRODUCTS_AND_CATEGORIES } from '../actions/appActionTypes';

const initialState = {
  dataFetched: false,
  categories: {},
  products: {},
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCTS_AND_CATEGORIES:
      return {
        ...state,
        dataFetched: true,
        categories: action.payload.categories,
        products: action.payload.products,
      };
    default:
      return state;
  }
}
