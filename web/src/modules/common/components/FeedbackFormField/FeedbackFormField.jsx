import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Form, Input } from 'semantic-ui-react';

import {
  getFeedbackIconClassname,
  validateCurrentValue,
} from './FeedbackFormField.helpers';

import {
  fieldDataShape,
  fieldDataFeedbackType,
} from '../../helpers/fieldDataHelpers';

class FeedbackFormField extends Component {
  onChange = (currentValue) => {
    const errors = validateCurrentValue(currentValue, this.props.data.type);
    const data = {
      ...this.props.data,
      feedback: [],
    };
    data.currentValue = currentValue;
    if (errors.length > 0) {
      data.feedback = errors.map(error => ({
        type: fieldDataFeedbackType.FRONT_END_ERROR,
        message: error
      }));
    }
    this.props.onChange(data);
  };

  render() {
    const {
      label, name, data, readOnly, fluid, disabled, width, type,
    } = this.props;

    return (
      <Form.Field
        width={width}
        error={
          data.feedback.some(feedback => (
            feedback.type === fieldDataFeedbackType.FRONT_END_ERROR ||
            feedback.type === fieldDataFeedbackType.BACK_END_ERROR
          ))
        }
      >
        {label &&
          <label htmlFor={_.kebabCase(name)}>
            {label}
          </label>
        }
        <Input
          id={_.kebabCase(name)}
          name={name}
          icon={!readOnly ? getFeedbackIconClassname(data) : undefined}
          loading={data.feedback.some(feedback => feedback.type === fieldDataFeedbackType.PROCESSING)}
          readOnly={readOnly}
          fluid={fluid}
          value={data.currentValue}
          disabled={disabled}
          type={type}
          onChange={e => this.onChange(_.trimStart(e.target.value))}
        />
        {data.feedback.some(({ message }) => !!message) &&
          <label
            htmlFor={_.kebabCase(name)}
          >
            {data.feedback.map(({ message }) => <p key={message}>{message}</p>)}
          </label>
        }
      </Form.Field>
    );
  }
}

FeedbackFormField.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  data: fieldDataShape.isRequired,
  onChange: PropTypes.func,
  readOnly: PropTypes.bool,
  fluid: PropTypes.bool,
  width: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string,
};

FeedbackFormField.defaultProps = {
  label: null,
  readOnly: false,
  fluid: false,
  width: undefined,
  disabled: false,
  type: 'text',
  onChange: () => _.noop,
};

export default FeedbackFormField;
