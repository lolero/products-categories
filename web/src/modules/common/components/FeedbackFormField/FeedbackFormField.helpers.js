import { fieldDataFeedbackType, fieldDataType } from '../../helpers/fieldDataHelpers';

export const getFeedbackIconClassname = (fieldData) => {
  if (!fieldData.feedback || fieldData.feedback.length < 1) return 'pencil';
  if (fieldData.feedback.some(({ type }) => (
    type === fieldDataFeedbackType.FRONT_END_ERROR ||
    type === fieldDataFeedbackType.BACK_END_ERROR
  ))) return 'warning circle';
  if (fieldData.feedback.some(({ type }) => type === fieldDataFeedbackType.SUCCESS)) return 'check';
  return 'pencil';
};

export const validateCurrentValue = (currentValue, type) => {
  const errors = [];
  switch (type) {
    case fieldDataType.NAME:
      if (currentValue.length < 1) errors.push('Name cannot be empty');
      break;
    default: break;
  }

  return errors;
};
