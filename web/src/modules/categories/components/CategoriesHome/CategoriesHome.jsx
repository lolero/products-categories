import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {Button, Header, Icon, Item, Modal, Segment} from 'semantic-ui-react';
import { connect } from 'react-redux';

import { deleteCategory } from '../../../common/services/categoriesServices';
import { fetchProductsAndCategories } from '../../../common/redux/actions/appActions';

class CategoriesHome extends Component {
  createNewCategory() {
    this.props.history.push('/categories/new');
  }

  editCategory(categoryId) {
    this.props.history.push(`/categories/edit/${categoryId}`);
  }

  async deleteCategory(categoryId) {
    await deleteCategory(categoryId);
    this.props.fetchProductsAndCategories();
  }

  render() {
    return (
      <React.Fragment>
        <Segment basic>
          <Button
            primary
            icon
            labelPosition='left'
            onClick={() => this.createNewCategory()}
          >
            <Icon name='plus' />
            New Category
          </Button>
        </Segment>
        <Segment basic>
          <Item.Group divided>
            {Object.values(this.props.categories).map(category => (
              <Item key={category.id}>
                <Item.Content>
                  <Item.Header>{category.name}</Item.Header>
                  <Item.Description>{category.description}</Item.Description>
                  <Item.Extra>
                    <Button
                      icon
                      labelPosition='left'
                      onClick={() => this.editCategory(category.id)}
                    >
                      <Icon name='pencil' />
                      Edit
                    </Button>
                    {
                      category.productIds.length < 1 &&
                      <Modal
                        trigger={
                          <Button icon labelPosition='left'>
                            <Icon name='delete' />
                            Delete
                          </Button>
                        }
                        size="mini"
                        closeIcon
                      >
                        <Header content='Delete Category' />
                        <Modal.Content>
                          Are you sure you want to delete category {category.name}?
                        </Modal.Content>
                        <Modal.Actions>
                          <Button onClick={() => this.deleteCategory(category.id)}>
                            Yes
                          </Button>
                        </Modal.Actions>
                      </Modal>
                    }
                  </Item.Extra>
                </Item.Content>
              </Item>
            ))}
          </Item.Group>
        </Segment>
      </React.Fragment>
    );
  }
}

CategoriesHome.propTypes = {
  history: PropTypes.object.isRequired,
  categories: PropTypes.object.isRequired,
  fetchProductsAndCategories: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  categories: state.appReducer.categories,
});

export default connect(
  mapStateToProps,
  {
    fetchProductsAndCategories,
  },
)(withRouter((CategoriesHome)));