import React from 'react';
import {Route, Switch} from 'react-router-dom';

import CategoriesHome from '../CategoriesHome/CategoriesHome';
import CategoryAddEdit from '../CategoryAddEdit/CategoryAddEdit';

const Categories = () => (
  <Switch>
    <Route exact path="/categories" component={CategoriesHome} />
    <Route exact path="/categories/new" component={CategoryAddEdit} />
    <Route exact path="/categories/edit/:id" component={CategoryAddEdit} />
  </Switch>
);

export default Categories;