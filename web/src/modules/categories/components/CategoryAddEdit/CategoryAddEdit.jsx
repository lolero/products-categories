import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import {Button, Form, Header, Segment} from 'semantic-ui-react';

import FeedbackFormField from '../../../common/components/FeedbackFormField/FeedbackFormField';
import { postCategory } from '../../../common/services/categoriesServices';
import {
  fieldDataType,
  getNewFieldsData,
  newFieldsDataActionType,
} from '../../../common/helpers/fieldDataHelpers';
import connect from 'react-redux/es/connect/connect';
import { fetchProductsAndCategories } from '../../../common/redux/actions/appActions';

class CategoryAddEdit extends Component {
  constructor(props) {
    super(props);
    const name = props.match.params.id ? props.categories[props.match.params.id].name : '';
    const description = props.match.params.id ? props.categories[props.match.params.id].description : '';
    const fieldsData = {
      name: {
        type: fieldDataType.NAME,
          savedValue: name,
          currentValue: name,
          feedback: [],
      },
      description: {
        type: fieldDataType.STRING,
          savedValue: description,
          currentValue: description,
          feedback: [],
      }
    };
    this.state = { fieldsData };
  };

  async saveCategory() {
    const fieldsData = getNewFieldsData(this.state.fieldsData, newFieldsDataActionType.PROCESSING);
    if (fieldsData) {
      this.setState({ fieldsData });
      if (this.props.match.params.id) fieldsData.id = { currentValue: this.props.match.params.id};
      const response = await postCategory(fieldsData);
      if (response.status === 201) {
        this.props.fetchProductsAndCategories();
        this.props.history.push('/categories');
      } else {
        this.setState(state => ({
          fieldsData: getNewFieldsData(
            state.fieldsData,
            newFieldsDataActionType.BACK_END_ERROR,
            response.data,
          )
        }));
      }
    }
  };

  render() {
    return (
      <Segment basic>
        <Header>{this.props.match.params.id ? 'Edit' : 'New'} Category</Header>
        <Form>
          {_.reject(Object.keys(this.state.fieldsData), fieldName => fieldName === 'id').map(fieldName => (
            <FeedbackFormField
              key={fieldName}
              label={_.upperFirst(fieldName)}
              name={fieldName}
              data={this.state.fieldsData[fieldName]}
              onChange={fieldData => this.setState(state => ({
                fieldsData: {
                  ...state.fieldsData,
                  [fieldName]: fieldData
                }
              }))}
            />
          ))}
          <Button
            primary
            onClick={() => this.saveCategory()}
          >
            Submit
          </Button>
        </Form>
      </Segment>
    );
  }
}

CategoryAddEdit.propTypes = {
  history: PropTypes.any.isRequired,
  match: PropTypes.object.isRequired,
  categories: PropTypes.object.isRequired,
  fetchProductsAndCategories: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  categories: state.appReducer.categories,
});

export default connect(
  mapStateToProps,
  {
    fetchProductsAndCategories,
  },
)(withRouter(CategoryAddEdit));
