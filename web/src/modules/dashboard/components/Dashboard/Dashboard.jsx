import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Container, Dimmer, Loader, Menu } from 'semantic-ui-react';
import { connect } from "react-redux";
import { fetchProductsAndCategories } from '../../../common/redux/actions/appActions';
import { withRouter } from 'react-router-dom';

class Dashboard extends Component {
  componentDidMount() {
    this.props.fetchProductsAndCategories();
  }

  render() {
    return (
      <Container>
        <Menu attached>
          {this.props.renderNavMenu()}
        </Menu>
        <div>
          {
            this.props.dataFetched ? (
              this.props.renderContent()
            ) : (
              <Dimmer active inverted>
                <Loader inverted content='Loading' />
              </Dimmer>
            )
          }
        </div>
      </Container>
    );
  }
}

Dashboard.propTypes = {
  renderNavMenu: PropTypes.func.isRequired,
  renderContent: PropTypes.func.isRequired,
  dataFetched: PropTypes.bool.isRequired,
  fetchProductsAndCategories: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  dataFetched: state.appReducer.dataFetched,
});

export default withRouter(connect(
  mapStateToProps,
  {
    fetchProductsAndCategories,
  },
)(Dashboard));
