import { combineReducers } from "redux";
import appReducer from "../../common/redux/reducers/appReducer";

export default combineReducers({
  appReducer,
});
