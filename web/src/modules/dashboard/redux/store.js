import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer';

const initalState = {};
const middleware = [thunk];

let store;

const ReactReduxDevTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

store = createStore(
  rootReducer,
  initalState,
  compose(
    applyMiddleware(...middleware),
    ReactReduxDevTools
  )
);

export default store;
